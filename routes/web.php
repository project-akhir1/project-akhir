<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layouts.master');
});

// Route::get('/login', function () {
//     return view('layouts.master');
// });

// CRUD Login & Sign Up
Route::get('/signup', 'SignUpController@signup' );
Route::get('/login', 'SignUpController@login' );

// CRUD MATKUL 
Route::get('/matkul/create', 'MatkulController@create' );
Route::post('/matkul', 'MatkulController@store' );
Route::get('/matkul', 'MatkulController@index' );
Route::get('/matkul/{matkul_id}', 'MatkulController@show' );
Route::get('/matkul/{matkul_id}/edit', 'MatkulController@edit' );
Route::put('/matkul/{matkul_id}', 'MatkulController@update' );
Route::delete('/matkul/{matkul_id}', 'MatkulController@destroy' );

// CRUD PERTANYAAN
Route::get('/pertanyaan/create', 'PertanyaanController@create');
Route::post('/pertanyaan', 'PertanyaanController@store');
Route::get('/pertanyaan', 'PertanyaanController@index');
Route::get('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@show');
Route::get('/pertanyaan/{pertanyaan_id}/edit', 'PertanyaanController@edit');
Route::put('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@update');
Route::delete('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@destroy');

// CRUD Profil
Route::get('/profil/create', 'ProfileController@create' );
Route::post('/profil', 'ProfileController@store' );
Route::get('/profil', 'ProfileController@index' );
Route::get('/profil/{matkul_id}', 'ProfileController@show' );
Route::get('/profil/{matkul_id}/edit', 'ProfileController@edit' );
Route::put('/profil/{matkul_id}', 'ProfileController@update' );
Route::delete('/profil/{matkul_id}', 'ProfileController@destroy' );

Route::resource('pertanyaan', 'PertanyaanController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
