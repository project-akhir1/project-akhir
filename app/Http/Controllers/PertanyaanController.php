<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\pertanyaan;
use App\matkul;


class PertanyaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pertanyaan = pertanyaan::all();
        return view('layouts.pertanyaan.index', compact('pertanyaan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $matkul = matkul::all();
        return view ('layouts.pertanyaan.create', compact('matkul'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            // 'nama' => 'required|unique:pertanyaan,nama|max:255',
            'pertanyaan' => 'required',
            'FotoPertanyaan' => 'required|mimes:png,jpg,jpeg',
            'matkul_id'=>'required',
        ]);

        $fileName = time().'.'.$request->FotoPertanyaan->extension();

        $matkul = new pertanyaan;

        $matkul->pertanyaan = $request->pertanyaan;
        $matkul->FotoPertanyaan = $fileName;
        $matkul->matkul_id = $request->matkul_id;

        $request->save();

        $request->FotoPertanyaan->move(public_path('images'), $fileName);

        return redirect('/pertanyaan/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pertanyaan = pertanyaan::findorfail($id);
        return view('layouts.pertanyaan.show', compact('pertanyaan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $matkul = matkul::all();
        $pertanyaan = pertanyaan::findorfail($id);
        return view('layouts.pertanyaan.edit', compact('pertanyaan','matkul'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
