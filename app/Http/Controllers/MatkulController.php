<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class MatkulController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $matkul = DB::table('matkul')->get();

        return view('layouts.matkul.index', compact('matkul'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('layouts.matkul.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|unique:matkul,nama|max:255',
        ],
        [
            'nama.required' => 'Mata kuliah harus diisi',
        ]);

        DB::table('matkul')->insert(
            [
                'nama'=> $request['nama']
            ]
        );
       
        return redirect('/matkul');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $matkul = DB::table('matkul')->where('id', $id)->first();
        return view('layouts.matkul.show', compact('matkul'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $matkul = DB::table('matkul')->where('id', $id)->first();
        return view('layouts.matkul.edit', compact('matkul'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required|unique:matkul,nama|max:255',
        ],
        [
            'nama.required' => 'Mata kuliah harus diisi',
        ]);

        DB::table('matkul')
            ->where('id', $id)
            ->update(
                [
                    'nama' => $request['nama'],
                ]
            );
       
        return redirect('/matkul');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('matkul')->where('id', '=', $id)->delete();

        return redirect('/matkul');
    }
}
