<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SignUpController extends Controller
{
    public function index(){
        return view('welcome', [
            ''
        ]);
    }

    public function signup(){
        return view('layouts.sign.signup');
    }

    public function login(){
        return view('layouts.sign.signin');
    }
}
