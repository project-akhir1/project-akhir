<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class pertanyaan extends Model
{
    protected $table = "pertanyaan";
    protected $fillable = ['pertanyaan','matkul_id'];
}
