<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
      <li class="nav-item">
        <a class="nav-link" href="/profil">
          <i class="icon-head menu-icon"></i>
          <span class="menu-title">Profile</span>
        </a>
      </li>
      {{-- <li class="nav-item">
        <a class="nav-link" href="/">
          <i class="icon-grid menu-icon"></i>
          <span class="menu-title">Dashboard</span>
        </a>
      </li> --}}
      <li class="nav-item">
        <a class="nav-link" href="/matkul">
          <i class="icon-paper menu-icon"></i>
          <span class="menu-title">Mata Kuliah</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/login">
          <i class="icon-power menu-icon"></i>
          <span class="menu-title">Logout</span>
        </a>
      </li>
    </ul>
</nav>