@extends('layouts.master')

@section('title')
    Edit Mata Kuliah {{$matkul->nama}}
@endsection

@section('content')
<form action="/matkul/{{$matkul->id}} " method="POST">
    @csrf
    @method('put')
    <div class="form-group">
        <label for="title">Nama Mata Kuliah</label>
        <input type="text" class="form-control" value="{{$matkul->nama}}" name="nama" id="title" placeholder="Masukkan nama">
        @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-success">Update</button>
</form>
@endsection