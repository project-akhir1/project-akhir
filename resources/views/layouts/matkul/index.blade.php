@extends('layouts.master')

@section('title')
    List Mata Kuliah
@endsection

@section('content')

<a href="/matkul/create" class="btn btn-success btn-sm">Tambah Mata Kuliah</a>

    <table class="table">
      <thead>
        <tr>
          <th>#</th>
          <th>Mata Kuliah</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        @forelse ($matkul as $key => $item)
            <tr>
                <td>{{$key +1}} </td>
                <td>{{$item->nama}} </td>
                <td>
                    <form action="/matkul/{{$item->id}}" method="POST">
                        @csrf
                        @method('delete')
                        <a href="/pertanyaan/create" class="btn btn-primary btn-sm">Pertanyaan</a>
                        <a href="/matkul/{{$item->id}}" class="btn btn-primary btn-sm">Edit</a>
                        <input type="submit" class="btn btn-danger btn-sm" value="delete">
                    </form>
                </td>
            </tr>

        @empty
            <tr>
                <td> Tidak Ada Data</td>
            </tr>
        @endforelse
      </tbody>
    </table>

@endsection