@extends('layouts.master')

@section('title')
    Edit Mata Kuliah {{$matkul->nama}}
@endsection

@section('content')
<p class="card-description">Masukkan Nama Mata Kuliah</p>
<form class="form-inline" action="/matkul/{{matkul->id}}" method="POST">
    @csrf
    @method('put')
    <label class="sr-only" for="title">nama</label>
    <input type="text" value ="{{$matkul->nama}}" class="form-control mb-2 mr-sm-2" id="title" name="nama" placeholder="Mata Kuliah">
    <button type="submit" class="btn btn-success">Update</button>
  </form>
@endsection