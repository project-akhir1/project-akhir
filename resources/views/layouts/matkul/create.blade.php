@extends('layouts.master')

@section('title')
    Mata Kuliah
@endsection

@section('content')
<p class="card-description">Masukkan Nama Mata Kuliah</p>
<form class="form-inline" action="/matkul" method="POST">
    @csrf
    <label class="sr-only" for="title">nama</label>
    <input type="text" class="form-control mb-2 mr-sm-2" id="title" name="nama" placeholder="Mata Kuliah">
    <button type="submit" class="btn btn-primary mb-2">Tambah</button>
  </form>
@endsection