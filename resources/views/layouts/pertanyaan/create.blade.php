@extends('layouts.master')

@section('title')
    Tuliskan Pertanyaan mu!
@endsection

@section('content')
<div class="col-md-6 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">

        <form class="forms-sample" action="/pertanyaan/create" method="POST" enchtype="multipart/form-data">
            @csrf
            @method('put')
            {{-- <div class="form-group">
              <label for="title" >Nama</label>
              <input type="text" class="form-control" id="title" name="nama" placeholder="Name">

            </div> --}}
            <div class="form-group">
              <label for="title" >Upload pertanyaanmu disini!</label>
              <input type="file" class="form-control" name="FotoPertanyaan" >
            </div>
            <div class="form-group">
              <label>Masukkan Penjelasan dari Pertanyaanmu</label>
              <textarea class="form-control" name="pertanyaan" rows="5"></textarea>
            </div>
            <div class="form-group">
                <label>Mata Kuliah</label>
                <select name="matkul_id" class="form-control" id="">
                  <option value="">-- Pilih Mata Kuliah --</option>
                  @foreach ($matkul as $item)
                      <option value="{{$item->id}}"> {{$item->nama}}</option>
                  @endforeach
                </select>
              </div>
             
            <button type="submit" class="btn btn-primary mr-2">Submit</button>
          </form>
      </div>
    </div>
  </div>
@endsection