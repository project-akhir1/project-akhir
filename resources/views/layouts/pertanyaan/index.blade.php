
            @extends('layouts.master')

@section('title')
    Pertanyaan
@endsection

@section('content')
  <div class="col-md-4 stretch-card grid-margin">
    <div class="card">
      <div class="card-body">
        <ul class="icon-data-list">
          <li>
            <a href="/pertanyaan/create" class="btn btn-info btn-sm">Tambah Pertanyaan</a>
            <div class="d-flex">
              @forelse ($pertanyaan as $item)
                <img src="{{asset('images/'.$item->FotoPertanyaan)}}" alt="user">
                  <div class="card">
                    <p class="text-info mb-1">{{$item->matkul_id}}</p>                    
                    <p class="mb-0">{{$item->pertanyaan}}</p>
                           <form action="{{route('pertanyaan.destroy',$item->id)}}" method="POST">
                      @csrf
                      @method('delete')
                      <a href="{{route('pertanyaan.show',$item->id)}}" class="btn btn-info btn-sm">Jawab</a>
                      <a href="{{route('pertanyaan.edit',$item->id)}}" class="btn btn-info btn-sm">Edit</a>
                      <input type="submit" class="btn btn-danger btn-sm" value="Delete"></input>
                    </form>
                  </div>

                    <button type="button" class="btn btn-outline-light btn-fw">Jawab</button>
                
                    </div>

                @empty
                  <label>Belum ada pertanyaan</label>
              @endforelse
            </div>
          </li>
        </ul>
      </div>
    </div>
  </div>
@endsection